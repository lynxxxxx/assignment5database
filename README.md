# Table of content
### 1 Background
### 2 Install
### 3 Run
### 4 Usage
### 5 Diagram
### 6 Maintainers

-----------------------------------------------------

### 1 Background
In order to construct a database, set up some tables in the database, add relationships to
the tables, and then fill the tables with data, you must create a number of scripts that can be executed.
Superheroes are crucial to both the database and its overall motif. SuperheroesDb is a possible name for the database.

### 2 Install
To set up the development environment you need Intellij
with at least JDK 17, PostgreSQL and PgAdmin installed on your PC

### 3 Run
YOu can run specific queries on PgAdmin or Intellij.

### 4 Usage
You must design the Superhero, Assistant, and Power tables, which together make up the three main tables.
A superhero has an autoincremented integer id, a name, an alias, and an origin.
id and Name of the assistant are auto-incremented integers.
id, Name, and Description are all autoincremented integers in Power.
 Make a script called 01 tableCreate.sql that contains instructions for setting up the primary keys for each of these tables.

One superhero may have several assistants, and each assistant may help one or more superheroes.
One superhero may possess a number of powers.

First, we need to add a foreign key to establish the superhero-assistant relationship. This is the superhero to which this assistant
is connected (SuperheroId in Assistant), and ALTER any tables required to construct the constraint and accommodate the foreign key.
REQUIREMENT: Create the script 02 relationshipSuperheroAssistant.sql, and include statements to ALTER any necessary tables to add 
the foreign key and set up the constraint to configure the described relationship between Superhero and assistant.

We need to add a linking table to establish the superhero-power relationship. The name of this table is up to you, but it should
be based on conventions. This table is purely for linking, meaning it should only contain two fields that are both foreign
keys and a composite primary key.

### 5 Diagram
You can see diagram in root of the project.

### 6 Maintainers
@DanSho
@lynxxxxx
@Enwynn 