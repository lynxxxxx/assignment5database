/*******************************************************************************
   Create Tables
********************************************************************************/


CREATE TABLE superhero
(
    superhero_id     serial PRIMARY KEY,
    superhero_name   VARCHAR(100) NOT NULL,
    superhero_alias  VARCHAR(100) NOT NULL,
    superhero_origin VARCHAR(100) NOT NULL
);

CREATE TABLE assistant
(
    assistant_id   serial PRIMARY KEY,
    assistant_name VARCHAR(100) NOT NULL
);

CREATE TABLE power
(
    power_id serial PRIMARY KEY,
    power_name VARCHAR(100) NOT NULL,
    power_description VARCHAR(100) NOT NULL
);
