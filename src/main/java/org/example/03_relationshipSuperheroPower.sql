
CREATE TABLE relationshipsuperheropower
(
    superhero_id int NOT NULL,
    power_id int NOT NULL,
    PRIMARY KEY (superhero_id, power_id)

);

ALTER TABLE relationshipsuperheropower
    ADD CONSTRAINT FK_relationshipsuperheropower_superhero
        FOREIGN KEY (superhero_id) REFERENCES superhero(superhero_id);

ALTER TABLE relationshipsuperheropower
    ADD CONSTRAINT FK_relationshipsuperheropower_power
        FOREIGN KEY (power_id) REFERENCES power(power_id);